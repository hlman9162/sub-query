Task 1:
(Scalar Subquery) Find employees who have a salary above the average salary of all employees.
Solution:
SELECT*FROM employees WHERE salary>(SELECT AVG(salary) FROM employees);

Task 2:
(Multiple-Row Subquery) Identify the employees who work in a department with more than 10 employees.
Solution:
SELECT first_name,
       last_name
FROM employees
WHERE department_id IN(
SELECT department_id 
FROM employees
GROUP BY department_id
HAVING COUNT(employee_id)>10);

Task 3:
(Scalar Subquery) Find employees who have been with the company longer than the average tenure.
Solution:
SELECT first_name,
       last_name
FROM employees
WHERE MONTHS_BETWEEN(sysdate,hire_date)>(
SELECT 
    AVG(MONTHS_BETWEEN(sysdate,hire_date))AS avg_tenure
FROM employees);

Task 4:
(Scalar Subquery) Discover employees who have a commission percentage above the average.
Solution:
SELECT*FROM employees
WHERE commission_pct >(
SELECT AVG(commission_pct) FROM employees);

Task 5:
(Correlated Subquery) Determine employees whose salary is higher than the average salary for their job title.
Solution:
SELECT 
     first_name,
     last_name,
     job_id,
     salary
FROM employees e
WHERE salary>(
 SELECT
     avg(salary)
     FROM employees
     WHERE e.job_id=job_id);
