Task 1:
(Correlated Subquery) Calculate the total number of employees and the total salary expenditure of the company.
Solution:
SELECT
   (SELECT COUNT(*) FROM employees) AS number_of_employees,
   (SELECT SUM(salary) FROM employees) AS total_expenditure
FROM dual;

Task 2:
(Correlated Subquery) Compute the maximum, minimum, and average salary across all departments.
Solution:
SELECT 
     MAX(e.salary),
     MIN(e.salary),
     AVG(e.salary),
     d.department_name
FROM employees e
INNER JOIN departments d
ON e.department_id=d.department_id
GROUP BY department_name;

Task 3:
(Correlated Subquery) Calculate the total number of departments and the average number of employees per department.
Solution:
SELECT
    (SELECT COUNT(department_id) FROM departments) AS number_of_departments,
    (SELECT AVG(COUNT(employee_id)) FROM employees GROUP BY department_id) AS number_of_employees
FROM dual;

Task 4:
(Correlated Subquery) Identify the total salary expenditure for each job title across the company.
Solution:
SELECT 
    j.job_id,
    j.job_title,
    (SELECT SUM(salary)
     FROM employees e
     WHERE e.job_id=j.job_id) AS total_expenditure
FROM jobs j;

Task 5:
(Correlated Subquery) Calculate the number of managers and the total number of their subordinates across the company.
Solution:
