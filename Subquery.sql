1.Single-Row Subqueries

--tek setir donduren subquery'dir

--task
--employee table'indaki en yuksek maas alan iscinin adini ve soyadini ekrana cixarmaq

SELECT first_name, last_name
FROM employees
WHERE salary = (SELECT MAX(salary) FROM employees);


--task
--60'ci departamentdə maaşı orta əmək haqqından yüksək olan bütün işçiləri tapmaq istəyirsiniz

SELECT *
FROM EMPLOYEES
WHERE salary > (
  SELECT AVG(salary)
  FROM EMPLOYEES
  WHERE department_id=60
);

SELECT *
FROM EMPLOYEES
WHERE salary > 3000,400,500;


2. Multiple-row Subqueries

--birden cox setir donduren subqery'lerdir

--task
--Məsələn, müəyyən bir şöbədəki bütün işçilərin adlarını və maaşlarını soruşan bir alt sorğu:

SELECT first_name, last_name, salary
FROM employees
WHERE department_id IN (SELECT department_id FROM departments WHERE location_id = 1700);
 10 20 30

3.Corrolated Subqery

--O, əsas sorğu əsasında işləyir və əsas sorğunun hər sətri üçün ayrıca qiymətləndirilir.
--Məsələn, hər bir işçinin öz departamentində orta əmək haqqından yüksək maaş alıb-almadığını yoxlayan sorğu:

SELECT first_name, last_name, salary
FROM employees e
WHERE salary > (SELECT AVG(salary) FROM employees WHERE department_id = e.department_id);


4.Scalar subquery

SELECT *
FROM PRODUCTS
WHERE stock_level < (
  SELECT MIN(stock_level)
  FROM PRODUCTS
);


5.Exists Subqueries:

--Onlar müəyyən bir şərtin mövcudluğunu yoxlayan alt sorğulardır.
--Məsələn, müəyyən bir şöbədə işçilərin olub olmadığını yoxlayan sorğu:

SELECT department_name
FROM departments d
WHERE EXISTS (SELECT 1 FROM employees e WHERE e.department_id = d.department_id);

