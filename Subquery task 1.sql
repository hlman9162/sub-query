Task 1:
(Scalar Subquery) Find employees who have a salary above the average salary of all employees.

Task 2:
(Multiple-Row Subquery) Identify the employees who work in a department with more than 10 employees.

Task 3:
(Scalar Subquery) Find employees who have been with the company longer than the average tenure.

Task 4:
(Scalar Subquery) Discover employees who have a commission percentage above the average.

Task 5:
(Correlated Subquery) Determine employees whose salary is higher than the average salary for their job title.

Task 6:
(Correlated Subquery) Calculate the total number of employees and the total salary expenditure of the company.

